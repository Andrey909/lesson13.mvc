<?php

/**
 * Created by PhpStorm.
 * User: hillel
 * Date: 13.12.17
 * Time: 21:05
 */
class Route
{
    static function start(){

        $segments = explode('/', $_SERVER['REQUEST_URI']);




        $controllerName = 'Main';
        $actionName = 'index';
        $params = [];

        if(!empty($segments[1])){
            $controllerName = $segments[1];
            $controllerName=ucfirst(strtolower($controllerName));


            if(!empty($segments[2])){
                $actionName = $segments[2];
                $actionName=ucfirst(strtolower($actionName));

                if(!empty($segments[3])){
                    $params = array_slice($segments, 3);
                    }

            }

        }


        $controllerFile = 'Controller_' . $controllerName;
        $actionName = 'action_' . $actionName;

        $controllerPath = 'application/controllers/' . $controllerFile . '.php';

        if(file_exists($controllerPath)){

            require_once $controllerPath;

        }else{
            Route::errorPage404();
        }

        $controller = new $controllerFile();

        if(method_exists($controller, $actionName)){

            $controller->$actionName($params);

        }else{
            Route::errorPage404();
        }

    }

    static function errorPage404(){
        echo 'Sorry, page not found';
        die();
    }
}